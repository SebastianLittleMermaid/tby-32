package com.cbx.tby.presenter.registration.contactuser.create;

import com.cbx.tby.presenter.Presenter;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserResponse;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class CreateContactUserPresenter implements Presenter<CreateContactUserView>, CreateContactUserResponse {

    private String result;

    @Override
    public CreateContactUserView createView() {
        CreateContactUserView view = new CreateContactUserView();
        view.bindViewModel(result);
        return view;
    }

    @Override
    public void response(final String contactUserId) {
        this.result = contactUserId;
    }
}
