package com.cbx.tby.presenter.registration.account.find;

import java.util.List;

import com.cbx.tby.presenter.View;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponseModel;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponseModel.Account;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class FindAccountView implements View<List<FindAccountResponseModel.Account>> {

    private List<Account> datas;
    
    @Override
    public View<List<Account>> bindViewModel(List<Account> viewModel) {
        datas = viewModel;
        return this;
    }

}
