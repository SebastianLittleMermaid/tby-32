package com.cbx.tby.presenter.registration.contactuser.find;

import com.cbx.tby.presenter.Presenter;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponse;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponseModel;

import java.util.List;

public class FindContactUserPresenter implements Presenter<FindContactUserView>, FindContactUserResponse {
    private List<FindContactUserResponseModel.ContactUser> resultList;

    @Override
    public FindContactUserView createView() {
        FindContactUserView view = new FindContactUserView();
        view.bindViewModel(resultList);
        return view;
    }

    @Override
    public void response(final List<FindContactUserResponseModel.ContactUser> user) {
        this.resultList = user;
    }
}
