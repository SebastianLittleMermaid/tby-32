package com.cbx.tby.presenter.mail.send;

import com.cbx.tby.presenter.View;
import com.cbx.tby.usecase.mail.contract.send.SendMailResponseModel;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class SendMailView implements View<SendMailResponseModel.Mail> {
    private SendMailResponseModel.Mail responseMessage;

    @Override
    public View<SendMailResponseModel.Mail> bindViewModel(SendMailResponseModel.Mail viewModel) {
        responseMessage = viewModel;
        return this;
    }
}
