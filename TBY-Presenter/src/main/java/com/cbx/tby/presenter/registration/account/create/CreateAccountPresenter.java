package com.cbx.tby.presenter.registration.account.create;

import com.cbx.tby.presenter.Presenter;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountResponse;

public class CreateAccountPresenter implements Presenter<CreateAccountView>, CreateAccountResponse {
	
	private String result;
	
	@Override
	public void response(String accountId) {
		this.result = accountId;
	}

	@Override
	public CreateAccountView createView() {
		CreateAccountView view = new CreateAccountView();
		view.bindViewModel(result);
		return view;
	}

}
