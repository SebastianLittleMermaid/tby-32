package com.cbx.tby.presenter.registration.account.create;


import com.cbx.tby.presenter.View;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class CreateAccountView implements View<String> {
    
    private String id;
    
    @Override
    public View<String> bindViewModel(String viewModel) {
        this.id = viewModel;
        return this;
    }

}
