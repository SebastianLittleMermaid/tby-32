package com.cbx.tby.presenter.mail.send;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.presenter.Presenter;
import com.cbx.tby.usecase.mail.contract.send.SendMailResponse;
import com.cbx.tby.usecase.mail.contract.send.SendMailResponseModel;

public class SendMailPresenter implements Presenter<SendMailView>, SendMailResponse {
    private SendMailResponseModel.Mail result;

    @Override
    public SendMailView createView() {
        SendMailView view = new SendMailView();
        view.bindViewModel(result);
        return view;
    }

    @Override
    public void response(Mail mail) {
        result = createResponseFromMailModel(mail);
    }

    private SendMailResponseModel.Mail createResponseFromMailModel(Mail mail) {
        return new SendMailResponseModel.Mail(
                mail.getSendTo(),
                mail.getSubject(),
                mail.getSendTime());
    }
}
