package com.cbx.tby.presenter.registration.contactuser.find;

import com.cbx.tby.presenter.View;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponseModel;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
public class FindContactUserView implements View<List<FindContactUserResponseModel.ContactUser>> {

    private List<FindContactUserResponseModel.ContactUser> contactUser;

    @Override
    public View<List<FindContactUserResponseModel.ContactUser>> bindViewModel(final List<FindContactUserResponseModel.ContactUser> viewModel) {
        contactUser = viewModel;
        return this;
    }
}
