package com.cbx.tby.presenter.registration.account.find;

import java.util.List;

import com.cbx.tby.presenter.Presenter;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponse;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponseModel;

public class FindAccountPresenter implements Presenter<FindAccountView>, FindAccountResponse {
	
	private List<FindAccountResponseModel.Account> datas;
	
	@Override
	public void response(List<FindAccountResponseModel.Account> data) {
		this.datas = data;
	}

	@Override
	public FindAccountView createView() {
		FindAccountView view = new FindAccountView();
		view.bindViewModel(datas);
		return view;
	}

}
