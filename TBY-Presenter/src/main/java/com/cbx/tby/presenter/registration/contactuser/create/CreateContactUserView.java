package com.cbx.tby.presenter.registration.contactuser.create;

import com.cbx.tby.presenter.View;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class CreateContactUserView implements View<String> {

    private String userId;

    @Override
    public View<String> bindViewModel(final String viewModel) {
        userId = viewModel;
        return this;
    }
}
