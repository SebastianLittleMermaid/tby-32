package com.cbx.tby.model.registration;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

/**
 * @author jam.hsu
 *
 */
@ToString
@Getter
@Setter
@Builder
public class Account {
    private String id;
    private String accountName;
    private String password;
    private List<ContactUser> user;
    private Role role;
    private Date createTime;
}
