package com.cbx.tby.model.mail;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@ToString
@Getter
@Setter
@Builder
public class Mail {
    private List<String> sendTo;
    private String sendFrom;
    private Date sendTime;
    private String subject;
    private String content;
}
