package com.cbx.tby.model.registration;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author jam.hsu
 *
 */
@ToString
@Getter
@Setter
@Builder
public class UserProfile {
    private String email;
    private String phoneNumber;
}
