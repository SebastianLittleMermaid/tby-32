package com.cbx.tby.model.registration;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Builder
public class ContactUser {
    private String id;
    private String accountId;
    private String name;
    private UserProfile profile;
}
