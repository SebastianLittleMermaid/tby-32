package com.cbx.tby.service.registration.contactuser.facade;

import com.cbx.tby.presenter.registration.contactuser.create.CreateContactUserPresenter;
import com.cbx.tby.presenter.registration.contactuser.find.FindContactUserPresenter;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserStory;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserStory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactUserService {
    @Autowired
    CreateContactUserStory createContactStory;

    @Autowired
    FindContactUserStory findContactUserStory;

    public void createContactUser(final CreateContactUserRequest request, final CreateContactUserPresenter presenter) {
        createContactStory.execute(request, presenter);
    }

    public void findContactUser(final FindContactUserRequest request, final FindContactUserPresenter presenter) {
        findContactUserStory.execute(request, presenter);
    }
}
