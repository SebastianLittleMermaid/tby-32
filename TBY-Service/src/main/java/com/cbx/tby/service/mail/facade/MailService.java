package com.cbx.tby.service.mail.facade;

import com.cbx.tby.presenter.mail.send.SendMailPresenter;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequest;
import com.cbx.tby.usecase.mail.contract.send.SendMailStory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailService {
    @Autowired
    private SendMailStory sendMailStory;

    public void sendMail(final SendMailRequest request, final SendMailPresenter presenter) {
        sendMailStory.execute(request, presenter);
    }
}
