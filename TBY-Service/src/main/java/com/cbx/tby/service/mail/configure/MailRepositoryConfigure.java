package com.cbx.tby.service.mail.configure;

import com.cbx.tby.usecase.mail.contract.send.SendMailStory;
import com.cbx.tby.usecase.mail.impl.send.SendMailStoryImpl;
import com.cbx.tby.usecase.mail.impl.send.SendMailTransferMapper;
import com.cbx.tby.usecase.mail.repository.MailRepository;
import com.cbx.tby.web.repository.mail.MailRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MailRepositoryConfigure {

    @Bean
    public MailRepository mailRepository() {
        return new MailRepositoryImpl();
    }

    @Bean
    SendMailStory sendMailStoryConfigure(final MailRepository repository) {
        SendMailTransferMapper mapper = new SendMailTransferMapper();
        return new SendMailStoryImpl(repository, mapper);
    }
}
