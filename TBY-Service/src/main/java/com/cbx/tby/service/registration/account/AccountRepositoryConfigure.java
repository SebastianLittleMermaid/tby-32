package com.cbx.tby.service.registration.account;

import com.cbx.tby.usecase.mail.contract.send.SendMailStory;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountUserStory;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountUserStory;
import com.cbx.tby.usecase.registration.account.contract.repository.AccountRepository;
import com.cbx.tby.usecase.registration.account.impl.create.CreateAccountTransferMapper;
import com.cbx.tby.usecase.registration.account.impl.create.CreateAccountUserStoryImpl;
import com.cbx.tby.usecase.registration.account.impl.find.FindAccountTransferMapper;
import com.cbx.tby.usecase.registration.account.impl.find.FindAccountUserStoryImpl;
import com.cbx.tby.web.repository.registration.AccountRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AccountRepositoryConfigure {
	
	@Bean 
	public AccountRepository accountRepository() {
		return new AccountRepositoryImpl();
	}

	@Bean
	public CreateAccountUserStory createAccountConfigure(
	        SendMailStory sendMail,
			AccountRepository repository) {
		return new CreateAccountUserStoryImpl(repository, sendMail, new CreateAccountTransferMapper());
	}
	
	@Bean
	public FindAccountUserStory findAccountConfigure(
			AccountRepository repository) {
		return new FindAccountUserStoryImpl(repository, new FindAccountTransferMapper());
	}
}
