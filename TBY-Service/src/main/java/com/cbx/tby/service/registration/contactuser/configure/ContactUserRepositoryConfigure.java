package com.cbx.tby.service.registration.contactuser.configure;

import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserStory;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserStory;
import com.cbx.tby.usecase.registration.contactuser.contract.repository.ContactUserRepository;
import com.cbx.tby.usecase.registration.contactuser.impl.create.CreateContactUserStoryImpl;
import com.cbx.tby.usecase.registration.contactuser.impl.create.mapper.CreateContactUserMapper;
import com.cbx.tby.usecase.registration.contactuser.impl.find.FindContactUserStoryImpl;
import com.cbx.tby.usecase.registration.contactuser.impl.find.mapper.FindContactUserMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ContactUserRepositoryConfigure {

    @Bean
    CreateContactUserStory createUserConfigura(final ContactUserRepository repository) {
        return new CreateContactUserStoryImpl(repository, new CreateContactUserMapper());
    }

    @Bean
    FindContactUserStory findContactorConfigure(final ContactUserRepository repository) {
        return new FindContactUserStoryImpl(repository, new FindContactUserMapper());
    }
}
