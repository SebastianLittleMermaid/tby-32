package com.cbx.tby.service.registration.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cbx.tby.presenter.registration.account.create.CreateAccountPresenter;
import com.cbx.tby.presenter.registration.account.find.FindAccountPresenter;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequest;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountUserStory;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountRequest;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountUserStory;

@Service
public class RegistrationService {
	
	@Autowired
	private FindAccountUserStory findAccount;
	
	@Autowired
	private CreateAccountUserStory createAccount;
	
	public void findAccount(FindAccountPresenter presenter) {
		FindAccountRequest request = new FindAccountRequest();
		findAccount.execute(request, presenter);
	}
	
	public void createAccount(CreateAccountRequest request, CreateAccountPresenter presenter) {
		createAccount.execute(request, presenter);
	}
}
