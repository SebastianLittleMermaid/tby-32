package com.cbx.tby.web.mail.controller;

import com.cbx.tby.presenter.mail.send.SendMailPresenter;
import com.cbx.tby.presenter.mail.send.SendMailView;
import com.cbx.tby.service.mail.facade.MailService;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailController {
    @Autowired
    private MailService service;

    @PostMapping(path = "/mail", consumes = "application/json", produces = "application/json")
    public SendMailView sendMail(@RequestBody SendMailRequest request) {
        SendMailPresenter presenter = new SendMailPresenter();
        service.sendMail(request, presenter);
        return presenter.createView();
    }
}
