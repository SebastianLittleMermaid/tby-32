package com.cbx.tby.web.registration.controller;

import com.cbx.tby.presenter.registration.account.create.CreateAccountPresenter;
import com.cbx.tby.presenter.registration.account.create.CreateAccountView;
import com.cbx.tby.presenter.registration.account.find.FindAccountPresenter;
import com.cbx.tby.presenter.registration.account.find.FindAccountView;
import com.cbx.tby.service.registration.account.RegistrationService;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {
	
	@Autowired
	private RegistrationService service;

	@GetMapping(path = "/account", produces = "application/json")
    public FindAccountView findAccount() {
        FindAccountPresenter presenter = new FindAccountPresenter();
        service.findAccount(presenter);
        return presenter.createView();
    }
    
    @PostMapping(path = "/account", consumes = "application/json", produces = "application/json")
    public CreateAccountView createAccount(@RequestBody CreateAccountRequest request) {
        CreateAccountPresenter presenter = new CreateAccountPresenter();
        service.createAccount(request, presenter);
        return presenter.createView();
    }
	
}
