package com.cbx.tby.web.registration.user.controller;

import com.cbx.tby.presenter.registration.contactuser.create.CreateContactUserPresenter;
import com.cbx.tby.presenter.registration.contactuser.create.CreateContactUserView;
import com.cbx.tby.presenter.registration.contactuser.find.FindContactUserPresenter;
import com.cbx.tby.presenter.registration.contactuser.find.FindContactUserView;
import com.cbx.tby.service.registration.contactuser.facade.ContactUserService;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class ContactUserController {
    @Autowired
    private ContactUserService service;

    @PostMapping(path = "/contactuser", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public CreateContactUserView createContactUser(@RequestBody final CreateContactUserRequest request){
        CreateContactUserPresenter presenter = new CreateContactUserPresenter();
        service.createContactUser(request, presenter);
        return presenter.createView();
    }

    @GetMapping(path = "/contactuser", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public FindContactUserView findContactUser(final FindContactUserRequest request) {
        FindContactUserPresenter presenter = new FindContactUserPresenter();
        service.findContactUser(request, presenter);
        return presenter.createView();
    }
}
