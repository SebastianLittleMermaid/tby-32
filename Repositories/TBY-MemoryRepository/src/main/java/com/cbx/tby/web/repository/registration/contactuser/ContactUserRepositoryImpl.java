package com.cbx.tby.web.repository.registration.contactuser;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.registration.contactuser.contract.repository.ContactUserRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Repository
public class ContactUserRepositoryImpl implements ContactUserRepository{
    private Map<String, ContactUser> dataMap = new HashMap<String, ContactUser>();

    public ContactUserRepositoryImpl() {
        initData();
    }

    private void initData() {
        String uuid = UUID.randomUUID().toString();
        final UserProfile userProfile = UserProfile.builder()
                .email("sebastian.wu@cbx.com")
                .phoneNumber("123456789")
                .build();

        final ContactUser sebastianWu = ContactUser.builder()
                .id(uuid)
                .accountId("Sebastian.Wu")
                .name("Sebastian Wu")
                .profile(userProfile)
                .build();

        dataMap.put(uuid, sebastianWu);

        uuid = UUID.randomUUID().toString();
        final UserProfile userProfile2 = UserProfile.builder()
          .email("jam.jsu@cbx.com")
          .phoneNumber("123456789")
          .build();

        final ContactUser jamHsu = ContactUser.builder()
          .id(uuid)
          .accountId("Jam.Hsu")
          .name("Jam Hsu")
          .profile(userProfile2)
          .build();

        dataMap.put(uuid, jamHsu);
    }

    @Override
    public ContactUser find(String id) {
        return dataMap.get(id);
    }

    @Override
    public List<ContactUser> findAll() {
        return new ArrayList<ContactUser>(dataMap.values());
    }

    @Override
    public String insert(ContactUser data) {
        final String insertUuid = UUID.randomUUID().toString();
        dataMap.put(insertUuid, data);
        return insertUuid;
    }

}
