package com.cbx.tby.web.repository.mail;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.usecase.mail.repository.MailRepository;

import java.util.*;

public class MailRepositoryImpl implements MailRepository {

    private Map<String, Mail> dataMap = new HashMap<>();

    public MailRepositoryImpl() {
        initData();
    }

    private void initData() {
        String uuid1 = UUID.randomUUID().toString();

        List<String> sendTo = new ArrayList<>();
        sendTo.add("alvin-1@cbx.com");
        sendTo.add("alvin-2@cbx.com");

        Mail mail = Mail.builder()
                .sendFrom("alvin@cbx.com")
                .sendTo(sendTo).sendTime(new Date())
                .subject("Registration")
                .content("registration success")
                .build();

        dataMap.put(uuid1, mail);
    }
    @Override
    public Mail find(String id) {
        return null;
    }

    @Override
    public List<Mail> findAll() {
        return null;
    }

    @Override
    public String insert(Mail data) {
        return null;
    }
}
