package com.cbx.tby.web.repository.registration;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.Role;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.registration.account.contract.repository.AccountRepository;

public class AccountRepositoryImpl implements AccountRepository {
	
	private Map<String, Account> dataMap = new HashMap<String, Account>();
	
	public AccountRepositoryImpl() {
		initData();
	}
	
	private void initData() {
		String uuid1 = UUID.randomUUID().toString();
		Role role = Role.builder()
				.name("Admin")
				.build();
		UserProfile userProfile = UserProfile.builder()
				.email("jam@cbx.com")
				.phoneNumber("123456789")
				.build();
		ContactUser user = ContactUser.builder()
				.name("Jam Hsu")
				.accountId(uuid1)
				.profile(userProfile).build();
		List<ContactUser> userList = new ArrayList<ContactUser>();
		userList.add(user);
		Account account = Account.builder()
				.id(uuid1)
				.accountName("CBX-TBY")
				.password("asldkfj;asd")
				.createTime(new Date())
				.role(role)
				.user(userList)
				.build();
		dataMap.put(uuid1, account);
	}

	@Override
	public Account find(String id) {
		return dataMap.get(id);
	}

	@Override
	public List<Account> findAll() {
		return new ArrayList<Account>(dataMap.values());
	}

	@Override
	public String insert(Account data) {
		String uuid = UUID.randomUUID().toString();
		data.setId(uuid);
		data.setCreateTime(new Date());
		data.getUser().forEach((u -> u.setAccountId(uuid)));
		dataMap.put(uuid, data);
		return uuid;
	}

}
