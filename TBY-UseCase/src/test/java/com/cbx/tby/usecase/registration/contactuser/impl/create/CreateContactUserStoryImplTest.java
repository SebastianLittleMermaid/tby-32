package com.cbx.tby.usecase.registration.contactuser.impl.create;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequestModel;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserResponse;
import com.cbx.tby.usecase.registration.contactuser.contract.repository.ContactUserRepository;
import com.cbx.tby.usecase.registration.contactuser.impl.create.mapper.CreateContactUserMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class CreateContactUserStoryImplTest {

    @Test
    void testExecute() {
        final UserProfile profile = UserProfile.builder()
                .email("newContactUser@cbx.com")
                .build();

        final ContactUser contactUser = ContactUser.builder()
                .accountId("NewContactUserId")
                .name("NewContactUserName")
                .profile(profile)
                .build();

        final CreateContactUserRequest request = Mockito.mock(CreateContactUserRequest.class);
        request.setContactUser(
                CreateContactUserRequestModel.ContactUser.builder()
                .accountId("NewContactUserId")
                .name("NewContactUserName")
                .build()
        );

        final ContactUserRepository repository = Mockito.mock(ContactUserRepository.class);
        when(repository.insert(any(ContactUser.class))).thenReturn("NewContactUserId");
        final CreateContactUserMapper mapper = Mockito.mock(CreateContactUserMapper.class);
        when(mapper.transfer(any(CreateContactUserRequest.class))).thenReturn(contactUser);

        final CreateContactUserStoryImpl story = new CreateContactUserStoryImpl(repository, mapper);
        story.execute(request, new CreateContactUserResponse() {
            @Override
            public void response(final String contactUserId) {
                Assert.assertEquals("NewContactUserId", contactUserId);
            }
        });
    }
}
