package com.cbx.tby.usecase.registration.account.impl.create;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequest;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequestModel;

class CreateAccountTransferMapperTest {

	@Test
	void test() {
		CreateAccountRequest data = createDefaultData();
		CreateAccountTransferMapper mapper = new CreateAccountTransferMapper();
		Account result = mapper.transfer(data);
		assertEquals(data.getAccountName(), result.getAccountName());
		assertEquals(data.getPassword(), data.getPassword());
		assertEquals(data.getRole().getName(), result.getRole().getName());
		assertEquals(data.getUser().size(), result.getUser().size());
	}
	
	private CreateAccountRequest createDefaultData() {
		CreateAccountRequestModel.Role role = CreateAccountRequestModel.Role.builder()
				.name("Admin")
				.build();
		CreateAccountRequestModel.UserProfile userProfile = CreateAccountRequestModel.UserProfile.builder()
				.email("jam@cbx.com")
				.phoneNumber("123456789")
				.build();
		CreateAccountRequestModel.ContactUser user = CreateAccountRequestModel.ContactUser.builder()
				.name("Jam Hsu")
				.profile(userProfile).build();
		List<CreateAccountRequestModel.ContactUser> userList = new ArrayList<CreateAccountRequestModel.ContactUser>();
		userList.add(user);
		CreateAccountRequest request = new CreateAccountRequest();
		request.setAccountName("TEST_ACCOUNT");
		request.setPassword("123");
		request.setRole(role);
		request.setUser(userList);
		return request;
	}


}
