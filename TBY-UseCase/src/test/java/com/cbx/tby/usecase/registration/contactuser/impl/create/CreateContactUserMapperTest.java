package com.cbx.tby.usecase.registration.contactuser.impl.create;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequestModel;
import com.cbx.tby.usecase.registration.contactuser.impl.create.mapper.CreateContactUserMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateContactUserMapperTest {
    @Test
    void test() {
        final CreateContactUserRequest request = createDefaultData();
        final CreateContactUserMapper mapper = new CreateContactUserMapper();
        final ContactUser contactUser = mapper.transfer(request);
        final UserProfile userProfile = contactUser.getProfile();
        final CreateContactUserRequestModel.UserProfile profile = request.getContactUser().getProfile();
        assertEquals(contactUser.getAccountId(), request.getContactUser().getAccountId());
        assertEquals(contactUser.getName(), request.getContactUser().getName());
        assertEquals(userProfile.getEmail(), profile.getEmail());
    }

    private CreateContactUserRequest createDefaultData() {
        final CreateContactUserRequestModel.UserProfile profile = CreateContactUserRequestModel.UserProfile.builder()
                .email("sebastian.wu@cbx.com").build();
        final CreateContactUserRequestModel.ContactUser contactUser = CreateContactUserRequestModel.ContactUser.builder()
                .accountId("Sebastian.Wu")
                .name("Sebastian Wu")
                .profile(profile)
                .build();

        final CreateContactUserRequest request = new CreateContactUserRequest();
        request.setContactUser(contactUser);
        return request;
    }
}
