package com.cbx.tby.usecase.registration.contactuser.impl.find;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponseModel;
import com.cbx.tby.usecase.registration.contactuser.impl.find.mapper.FindContactUserMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FindContactUserMapperTest {
    private final FindContactUserMapper mapper;

    public FindContactUserMapperTest() {
        mapper = new FindContactUserMapper();
    }

    @Test
    void test() {
        testTransfer();
        testReverse();
    }

    private void testTransfer() {
        // Test Transfer
        final FindContactUserRequest requestData = createDefaultRequestData();
        final ContactUser result = mapper.transfer(requestData);
        assertEquals(requestData.getId(), result.getAccountId());
    }

    private FindContactUserRequest createDefaultRequestData() {
        final FindContactUserRequest request = new FindContactUserRequest();
        request.setId("Sebastian.Wu");
        return request;
    }

    private void testReverse() {
        // Test Reverse
        final ContactUser responseData = createDefaultResponseData();
        final FindContactUserResponseModel.ContactUser result = mapper.reverse(responseData);
        assertEquals(responseData.getAccountId(), result.getAccountId());
    }

    private ContactUser createDefaultResponseData() {
        final UserProfile userProfile = UserProfile.builder().email("sebastian.wu@cbx.com").build();
        return ContactUser.builder()
                .accountId("Sebastian.Wu")
                .name("Sebastian Wu")
                .profile(userProfile)
                .build();
    }
}
