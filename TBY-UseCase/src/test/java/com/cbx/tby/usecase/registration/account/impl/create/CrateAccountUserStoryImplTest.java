package com.cbx.tby.usecase.registration.account.impl.create;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.usecase.mail.contract.send.SendMailStory;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequest;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountResponse;
import com.cbx.tby.usecase.registration.account.contract.repository.AccountRepository;

class CrateAccountUserStoryImplTest {

	@Test
	void testExecute() {
        Account account = Mockito.mock(Account.class);

		AccountRepository repository = Mockito.mock(AccountRepository.class);
		when(repository.insert(any(Account.class))).thenReturn("NewAccount-1");

		CreateAccountTransferMapper mapper = Mockito.mock(CreateAccountTransferMapper.class);
		when(mapper.transfer(any(CreateAccountRequest.class))).thenReturn(account);

        CreateAccountUserStoryImpl story = new CreateAccountUserStoryImpl(repository, mockSendMailStory(), mapper);
		story.execute(new CreateAccountRequest(), new CreateAccountResponse() {

			@Override
			public void response(String accountId) {
				assertEquals("NewAccount-1", accountId);
			}
		});
	}
	
    private SendMailStory mockSendMailStory() {
        SendMailStory story = Mockito.mock(SendMailStory.class);
        return story;
    }

}

