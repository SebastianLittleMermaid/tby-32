package com.cbx.tby.usecase.registration.account.impl.find;


import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountRequest;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponse;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponseModel;
import com.cbx.tby.usecase.registration.account.contract.repository.AccountRepository;

class FindAccountUserStoryImplTest {

	@Test
	void testExecute() {
		// mock data
		Account mockAccount1 = Account.builder().accountName("mock1").build();
		Account mockAccount2 = Account.builder().accountName("mock2").build();
		FindAccountResponseModel.Account mockResponseAccount = FindAccountResponseModel.Account.builder()
				.accountName("mock")
				.build();
		AccountRepository repository = Mockito.mock(AccountRepository.class);
		when(repository.find(any(String.class))).thenReturn(mockAccount1);
		
		List<Account> mockDataList = new ArrayList<Account>();
		mockDataList.add(mockAccount1);
		mockDataList.add(mockAccount2);
		when(repository.findAll()).thenReturn(mockDataList);
		
		// mock mapper
		FindAccountTransferMapper mapper = Mockito.mock(FindAccountTransferMapper.class);
		when(mapper.transfer(any(Account.class))).thenReturn(mockResponseAccount);
		
		// mock request
		FindAccountRequest mockRequest = Mockito.mock(FindAccountRequest.class);
		when(mockRequest.getId()).thenReturn("account-123");
		
		// start test
		FindAccountUserStoryImpl story = new FindAccountUserStoryImpl(repository, mapper);
		story.execute(mockRequest, new FindAccountResponse() {

			@Override
			public void response(
					List<FindAccountResponseModel.Account> data) {
				assertEquals(1, data.size());
			}
		});
		
		story.execute(new FindAccountRequest(), new FindAccountResponse() {

			@Override
			public void response(
					List<FindAccountResponseModel.Account> data) {
				assertEquals(2, data.size());
			}
		});

		
	}

}
