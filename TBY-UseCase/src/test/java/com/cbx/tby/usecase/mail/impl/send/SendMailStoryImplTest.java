package com.cbx.tby.usecase.mail.impl.send;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequest;
import com.cbx.tby.usecase.mail.contract.send.SendMailResponse;
import com.cbx.tby.usecase.mail.repository.MailRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class SendMailStoryImplTest {

    @Test
    void testExecute() {
        List<String> sendTo = new ArrayList<>();
        sendTo.add("alvin-1@cbx.com");
        sendTo.add("alvin-2@cbx.com");

        Mail mail = Mail.builder()
                .sendFrom("alvin@cbx.com")
                .sendTo(sendTo)
                .sendTime(new Date())
                .subject("Registration")
                .content("registration success")
                .build();

        MailRepository repository = Mockito.mock(MailRepository.class);

        SendMailTransferMapper mapper = Mockito.mock(SendMailTransferMapper.class);
        when(mapper.transfer(any(SendMailRequest.class))).thenReturn(mail);

        SendMailStoryImpl story = new SendMailStoryImpl(repository, mapper);
        story.execute(new SendMailRequest(), new SendMailResponse() {

            @Override
            public void response(Mail requestMail) {
                assertEquals(requestMail.getSubject(), mail.getSubject());
            }
        });
    }
}
