package com.cbx.tby.usecase.registration.account.impl.find;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.Role;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponseModel;

class FindAccountTransferMapperTest {

	@Test
	void test() {
		Account data = createDefaultData();
		FindAccountTransferMapper mapper = new FindAccountTransferMapper();
		FindAccountResponseModel.Account result = mapper.transfer(data);
		assertEquals(data.getAccountName(), result.getAccountName());
		assertEquals(data.getPassword(), data.getPassword());
		assertEquals(data.getRole().getName(), result.getRole().getName());
		assertEquals(data.getUser().size(), result.getUser().size());
	}
	
	private Account createDefaultData() {
		String uuid1 = UUID.randomUUID().toString();
		Role role = Role.builder()
				.name("Admin")
				.build();
		UserProfile userProfile = UserProfile.builder()
				.email("jam@cbx.com")
				.phoneNumber("123456789")
				.build();
		ContactUser user = ContactUser.builder()
				.name("Jam Hsu")
				.accountId(uuid1)
				.profile(userProfile).build();
		List<ContactUser> userList = new ArrayList<ContactUser>();
		userList.add(user);
		Account account = Account.builder()
				.id(uuid1)
				.accountName("CBX-TBY")
				.password("asldkfj;asd")
				.createTime(new Date())
				.role(role)
				.user(userList)
				.build();
		return account;
	}


}
