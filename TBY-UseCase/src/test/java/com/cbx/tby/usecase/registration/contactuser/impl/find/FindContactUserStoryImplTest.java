package com.cbx.tby.usecase.registration.contactuser.impl.find;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponse;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponseModel;
import com.cbx.tby.usecase.registration.contactuser.contract.repository.ContactUserRepository;
import com.cbx.tby.usecase.registration.contactuser.impl.find.mapper.FindContactUserMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class FindContactUserStoryImplTest {
    private final ContactUser sebastian;
    private final ContactUser jam;
    private final List<ContactUser> contactUsersList;
    private final FindContactUserResponseModel.ContactUser responseContactUserSebastian;

    public FindContactUserStoryImplTest() {
        sebastian = ContactUser.builder()
          .accountId("Sebastian.Wu")
          .name("Sebastian Wu")
          .build();

        jam = ContactUser.builder()
          .accountId("Jam.Hsu")
          .name("Jam Hsu")
          .build();

        contactUsersList =
          new ArrayList<ContactUser>();
        contactUsersList.add(sebastian);
        contactUsersList.add(jam);

        responseContactUserSebastian =
          FindContactUserResponseModel.ContactUser.builder()
            .accountId("Sebastian.Wu")
            .name("sebastian wu")
            .build();
    }

    @Test
    void testExecute() {
        // Mock Repository
        final ContactUserRepository repository = Mockito.mock(ContactUserRepository.class);
        when(repository.findAll()).thenReturn(contactUsersList);
        when(repository.find(any(String.class))).thenReturn(sebastian);

        // Mock Mapper
        final FindContactUserMapper mapper = Mockito.mock(FindContactUserMapper.class);
        when(mapper.transfer(any(FindContactUserRequest.class))).thenReturn(sebastian);
        when(mapper.reverse(any(ContactUser.class))).thenReturn(responseContactUserSebastian);

        // Mock Request
        final FindContactUserRequest request = Mockito.mock(FindContactUserRequest.class);
        when(request.getId()).thenReturn("1111-0000-2222-444");

        // Start Test - find
        FindContactUserStoryImpl story = new FindContactUserStoryImpl(repository, mapper);
        story.execute(request, new FindContactUserResponse() {
            @Override
            public void response(final List<FindContactUserResponseModel.ContactUser> user) {
                assertTrue(user.size() > 0);
            }
        });

        // Start Test - findAll
        story.execute(new FindContactUserRequest(), new FindContactUserResponse() {
            @Override
            public void response(final List<FindContactUserResponseModel.ContactUser> user) {
                assertTrue(user.size() > 1);
            }
        });
    }
}
