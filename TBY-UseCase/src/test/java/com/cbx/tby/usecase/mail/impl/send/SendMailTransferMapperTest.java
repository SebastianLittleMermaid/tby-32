package com.cbx.tby.usecase.mail.impl.send;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequest;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequestModel;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SendMailTransferMapperTest {
    @Test
    void testExecute() {
        SendMailRequest requestData = createDefaultData();
        SendMailTransferMapper mapper = new SendMailTransferMapper();
        Mail result = mapper.transfer(requestData);
        SendMailRequestModel.Mail requestMail = requestData.getMail();
        assertEquals(requestMail.getSendTo(), result.getSendTo());
        assertEquals(requestMail.getSubject(), result.getSubject());
        assertEquals(requestMail.getSendTime(), result.getSendTime());
    }

    private SendMailRequest createDefaultData() {
        List<String> sendTo = new ArrayList<>();
        sendTo.add("alvin-1@cbx.com");
        sendTo.add("alvin-2@cbx.com");

        SendMailRequestModel.Mail mail = SendMailRequestModel.Mail.builder()
                .sendTo(sendTo)
                .subject("test")
                .sendTime(new Date())
                .build();
        SendMailRequest request = new SendMailRequest();
        request.setMail(mail);
        return request;
    }
}
