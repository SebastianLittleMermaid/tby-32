package com.cbx.tby.usecase.registration.account.contract.find;

import java.util.List;

import com.cbx.tby.usecase.generic.contract.base.Response;

public interface FindAccountResponse extends Response<List<FindAccountResponseModel.Account>> {
}
