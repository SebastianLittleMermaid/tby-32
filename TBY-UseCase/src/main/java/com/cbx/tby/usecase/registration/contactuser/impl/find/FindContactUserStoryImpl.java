package com.cbx.tby.usecase.registration.contactuser.impl.find;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponse;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponseModel;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserStory;
import com.cbx.tby.usecase.registration.contactuser.contract.repository.ContactUserRepository;
import com.cbx.tby.usecase.registration.contactuser.impl.find.mapper.FindContactUserMapper;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class FindContactUserStoryImpl implements FindContactUserStory {

    private ContactUserRepository repository;
    private FindContactUserMapper mapper;

    public FindContactUserStoryImpl(final ContactUserRepository repository,
                             final FindContactUserMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public void execute(final FindContactUserRequest request, final FindContactUserResponse response) {
        final List<FindContactUserResponseModel.ContactUser> resultList =
          new ArrayList<FindContactUserResponseModel.ContactUser>();

        if (StringUtils.isNotBlank(request.getId())) {
            resultList.add(findContactUser(request));
        } else {
            resultList.addAll(findAllContactUser());
        }

        response.response(resultList);
    }

    private FindContactUserResponseModel.ContactUser findContactUser(final FindContactUserRequest request) {
        final ContactUser user = repository.find(mapper.transfer(request).getAccountId());
        return mapper.reverse(user);
    }

    private List<FindContactUserResponseModel.ContactUser> findAllContactUser() {
        final List<FindContactUserResponseModel.ContactUser> resultList =
          new ArrayList<FindContactUserResponseModel.ContactUser>();
        final List<ContactUser> userList = repository.findAll();
        userList.forEach(
            u -> resultList.add(mapper.reverse(u))
        );

        return resultList;
    }
}
