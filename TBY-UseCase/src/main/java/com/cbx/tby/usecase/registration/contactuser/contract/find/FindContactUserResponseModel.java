package com.cbx.tby.usecase.registration.contactuser.contract.find;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public interface FindContactUserResponseModel {
    @ToString
    @Getter
    @Setter
    @Builder
    class ContactUser {
        private String id;
        private String accountId;
        private String name;
        private UserProfile profile;
    }

    @ToString
    @Getter
    @Setter
    @Builder
    class UserProfile {
        private String email;
    }
}
