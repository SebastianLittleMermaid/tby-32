package com.cbx.tby.usecase.registration.contactuser.contract.create;

import com.cbx.tby.usecase.generic.contract.base.Response;

public interface CreateContactUserResponse extends Response<String> {
}
