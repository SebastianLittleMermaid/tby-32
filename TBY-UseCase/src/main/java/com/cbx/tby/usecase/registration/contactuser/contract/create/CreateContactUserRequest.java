package com.cbx.tby.usecase.registration.contactuser.contract.create;

import com.cbx.tby.usecase.generic.contract.base.Request;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class CreateContactUserRequest implements Request {
    private CreateContactUserRequestModel.ContactUser contactUser;
}
