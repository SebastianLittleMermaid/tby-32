package com.cbx.tby.usecase.mail.repository;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.usecase.generic.contract.repository.Repository;

public interface MailRepository extends Repository<Mail> {

}
