package com.cbx.tby.usecase.mail.contract.send;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

public interface SendMailResponseModel {

    @AllArgsConstructor
    @ToString
    @Getter
    @Builder
    class Mail {
        List<String> sendTo;
        String subject;
        Date sendTime;
    }
}
