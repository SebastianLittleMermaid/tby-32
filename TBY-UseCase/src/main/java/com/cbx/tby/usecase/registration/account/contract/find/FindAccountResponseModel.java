package com.cbx.tby.usecase.registration.account.contract.find;

import java.util.Date;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public interface FindAccountResponseModel {
	
	@ToString
	@Getter
	@Setter
	@Builder
	class Account {
		private String id;
	    private String accountName;
	    private String password;
	    private List<ContactUser> user;
	    private Role role;
	    private Date createTime;
	}
	
	@ToString
	@Getter
	@Setter
	@Builder
	class ContactUser {
		private String accountId;
		private String name;
	    private UserProfile profile;
	}
	
	@ToString
	@Getter
	@Setter
	@Builder
	class Role {
		private String name;
	}
	
	@ToString
	@Getter
	@Setter
	@Builder
	class UserProfile {
		private String email;
	    private String phoneNumber;
	}
}
