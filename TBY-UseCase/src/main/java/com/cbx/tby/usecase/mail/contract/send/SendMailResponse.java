package com.cbx.tby.usecase.mail.contract.send;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.usecase.generic.contract.base.Response;

public interface SendMailResponse extends Response<Mail>{
}
