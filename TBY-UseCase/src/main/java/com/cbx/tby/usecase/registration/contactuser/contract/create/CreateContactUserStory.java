package com.cbx.tby.usecase.registration.contactuser.contract.create;

import com.cbx.tby.usecase.generic.contract.base.Story;

public interface CreateContactUserStory extends
        Story<CreateContactUserRequest, CreateContactUserResponse> {
}
