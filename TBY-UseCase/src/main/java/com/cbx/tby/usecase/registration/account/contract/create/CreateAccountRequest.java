package com.cbx.tby.usecase.registration.account.contract.create;

import java.util.List;

import com.cbx.tby.usecase.generic.contract.base.Request;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequestModel.ContactUser;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequestModel.Role;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class CreateAccountRequest implements Request {
    private String accountName;
    private String password;
    private List<ContactUser> user;
    private Role role;
}
