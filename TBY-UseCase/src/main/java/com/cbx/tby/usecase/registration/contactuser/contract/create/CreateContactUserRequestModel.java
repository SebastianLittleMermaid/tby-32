package com.cbx.tby.usecase.registration.contactuser.contract.create;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

public interface CreateContactUserRequestModel {
    @ToString
    @Getter
    @Setter
    @Builder
    @JsonDeserialize(builder = ContactUser.ContactUserBuilder.class)
    class ContactUser {
        private String accountId;
        private String name;
        private UserProfile profile;
        @JsonPOJOBuilder(withPrefix = "")
        public static final class ContactUserBuilder {
        }
    }

    @ToString
    @Getter
    @Setter
    @Builder
    @JsonDeserialize(builder = UserProfile.UserProfileBuilder.class)
    class UserProfile {
        private String email;
        @JsonPOJOBuilder(withPrefix = "")
        public static final class UserProfileBuilder {
        }
    }
}
