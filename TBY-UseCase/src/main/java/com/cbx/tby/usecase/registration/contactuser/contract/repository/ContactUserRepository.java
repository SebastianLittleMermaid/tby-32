package com.cbx.tby.usecase.registration.contactuser.contract.repository;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.usecase.generic.contract.repository.Repository;

public interface ContactUserRepository extends Repository<ContactUser> {

}
