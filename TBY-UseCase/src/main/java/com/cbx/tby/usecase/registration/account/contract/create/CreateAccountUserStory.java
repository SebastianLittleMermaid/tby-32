package com.cbx.tby.usecase.registration.account.contract.create;

import com.cbx.tby.usecase.generic.contract.base.Story;

public interface CreateAccountUserStory extends Story<CreateAccountRequest, CreateAccountResponse> {
}
