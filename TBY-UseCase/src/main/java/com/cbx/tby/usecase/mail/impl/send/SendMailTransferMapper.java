package com.cbx.tby.usecase.mail.impl.send;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.usecase.generic.contract.base.TransferMapper;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequest;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequestModel;

public class SendMailTransferMapper implements TransferMapper<SendMailRequest, Mail> {
    @Override
    public Mail transfer(SendMailRequest request) {
        SendMailRequestModel.Mail mail = request.getMail();
        return Mail.builder()
                .sendTo(mail.getSendTo())
                .sendFrom(mail.getSendFrom())
                .sendTime(mail.getSendTime())
                .subject(mail.getSubject())
                .content(mail.getContent())
                .build();
    }
}