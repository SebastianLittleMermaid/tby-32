package com.cbx.tby.usecase.registration.account.contract.create;

import com.cbx.tby.usecase.generic.contract.base.Response;

public interface CreateAccountResponse extends Response<String> {
}
