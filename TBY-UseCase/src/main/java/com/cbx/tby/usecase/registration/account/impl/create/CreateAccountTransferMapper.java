package com.cbx.tby.usecase.registration.account.impl.create;

import java.util.ArrayList;
import java.util.List;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.Role;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.generic.contract.base.TransferMapper;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequest;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequestModel;

public class CreateAccountTransferMapper implements TransferMapper<CreateAccountRequest, Account> {

	@Override
	public Account transfer(CreateAccountRequest source) {
		Role role = Role.builder()
				.name(source.getRole().getName())
				.build();
		List<ContactUser> userList = transferUsers(source.getUser());
		return Account.builder()
				.accountName(source.getAccountName())
				.password(source.getPassword())
				.role(role)
				.user(userList)
				.build();
	}
	
	private List<ContactUser> transferUsers(List<CreateAccountRequestModel.ContactUser> userList) {
		List<ContactUser> results = new ArrayList<ContactUser>();
		for (CreateAccountRequestModel.ContactUser u : userList) {
			ContactUser transferResult = ContactUser.builder()
					.name(u.getName())
					.profile(transferProfile(u.getProfile()))
					.build();
			results.add(transferResult);
		}
		return results;
	}
	
	private UserProfile transferProfile(CreateAccountRequestModel.UserProfile profile) {
		return UserProfile.builder()
				.email(profile.getEmail())
				.phoneNumber(profile.getPhoneNumber())
				.build(); 
	}

}
