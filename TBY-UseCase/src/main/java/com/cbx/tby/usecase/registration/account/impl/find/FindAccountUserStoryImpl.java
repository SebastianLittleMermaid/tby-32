package com.cbx.tby.usecase.registration.account.impl.find;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountRequest;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponse;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponseModel;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountUserStory;
import com.cbx.tby.usecase.registration.account.contract.repository.AccountRepository;

public class FindAccountUserStoryImpl implements FindAccountUserStory {
	
	private AccountRepository accountRepository;
	private FindAccountTransferMapper mapper;
	
	public FindAccountUserStoryImpl(AccountRepository accountRepository, FindAccountTransferMapper mapper) {
		this.accountRepository = accountRepository;
		this.mapper = mapper;
	}

	@Override
	public void execute(FindAccountRequest request, FindAccountResponse response) {
		List<FindAccountResponseModel.Account> result = new ArrayList<FindAccountResponseModel.Account>();
		if (StringUtils.isBlank(request.getId())) {
			List<Account> accountList = accountRepository.findAll();
			for (Account account : accountList) {
				result.add(mapper.transfer(account));
			}
		} else {
			Account account = accountRepository.find(request.getId());
			result.add(mapper.transfer(account));
		}
		
		response.response(result);
	}

}
