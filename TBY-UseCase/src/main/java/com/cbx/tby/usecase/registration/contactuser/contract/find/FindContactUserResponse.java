package com.cbx.tby.usecase.registration.contactuser.contract.find;

import com.cbx.tby.usecase.generic.contract.base.Response;

import java.util.List;

public interface FindContactUserResponse extends Response<List<FindContactUserResponseModel.ContactUser>> {
}
