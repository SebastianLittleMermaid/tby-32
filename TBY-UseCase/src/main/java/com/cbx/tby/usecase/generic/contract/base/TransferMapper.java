package com.cbx.tby.usecase.generic.contract.base;

public interface TransferMapper<Source, Target> {
    Target transfer(Source source);
}
