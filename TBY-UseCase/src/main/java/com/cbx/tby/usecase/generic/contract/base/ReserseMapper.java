package com.cbx.tby.usecase.generic.contract.base;

public interface ReserseMapper<UseCaseModel, Model> {
    UseCaseModel reverse(Model model);
}
