package com.cbx.tby.usecase.generic.contract.base;

public interface Response<M> {
    void response(M data);
}
