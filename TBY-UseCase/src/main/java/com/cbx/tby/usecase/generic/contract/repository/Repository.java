package com.cbx.tby.usecase.generic.contract.repository;

import java.util.List;

public interface Repository<T> {
    T find(String id);

    List<T> findAll();

    String insert(T data); 
}
