package com.cbx.tby.usecase.generic.contract.base;

public interface Story<I extends Request, O extends Response<?>> {
    void execute(final I request, final O response);
}
