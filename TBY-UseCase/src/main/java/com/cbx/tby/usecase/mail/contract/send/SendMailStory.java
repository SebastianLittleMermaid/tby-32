package com.cbx.tby.usecase.mail.contract.send;

import com.cbx.tby.usecase.generic.contract.base.Story;

public interface SendMailStory extends Story<SendMailRequest, SendMailResponse>{
}
