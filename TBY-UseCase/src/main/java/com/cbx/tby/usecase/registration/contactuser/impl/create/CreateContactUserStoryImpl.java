package com.cbx.tby.usecase.registration.contactuser.impl.create;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserResponse;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserStory;
import com.cbx.tby.usecase.registration.contactuser.contract.repository.ContactUserRepository;
import com.cbx.tby.usecase.registration.contactuser.impl.create.mapper.CreateContactUserMapper;

import javax.annotation.Resource;

@Resource
public class CreateContactUserStoryImpl implements CreateContactUserStory {

    private ContactUserRepository repository;
    private CreateContactUserMapper mapper;

    public CreateContactUserStoryImpl(final ContactUserRepository repository,
                            final CreateContactUserMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public void execute(final CreateContactUserRequest request, final CreateContactUserResponse response) {
        final ContactUser contactUser = mapper.transfer(request);
        String accountId = repository.insert(contactUser);
        response.response(accountId);
    }
}
