package com.cbx.tby.usecase.mail.impl.send;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequest;
import com.cbx.tby.usecase.mail.contract.send.SendMailResponse;
import com.cbx.tby.usecase.mail.contract.send.SendMailStory;
import com.cbx.tby.usecase.mail.repository.MailRepository;

public class SendMailStoryImpl implements SendMailStory {
    private MailRepository repository;
    private SendMailTransferMapper mapper;

    public SendMailStoryImpl(final MailRepository repository, final SendMailTransferMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public void execute(SendMailRequest request, SendMailResponse response) {
        Mail mail = mapper.transfer(request);
        response.response(mail);
    }

}
