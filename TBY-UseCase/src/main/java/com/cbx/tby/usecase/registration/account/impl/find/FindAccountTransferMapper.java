package com.cbx.tby.usecase.registration.account.impl.find;

import java.util.ArrayList;
import java.util.List;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.generic.contract.base.TransferMapper;
import com.cbx.tby.usecase.registration.account.contract.find.FindAccountResponseModel;

public class FindAccountTransferMapper implements TransferMapper<Account, FindAccountResponseModel.Account> {

	@Override
	public FindAccountResponseModel.Account transfer(Account source) {
		FindAccountResponseModel.Role role = FindAccountResponseModel.Role.builder()
				.name(source.getRole().getName())
				.build();
		List<FindAccountResponseModel.ContactUser> userList = transferUsers(source.getUser());
		return FindAccountResponseModel.Account.builder()
				.id(source.getId())
				.accountName(source.getAccountName())
				.password(source.getPassword())
				.role(role)
				.user(userList)
				.createTime(source.getCreateTime())
				.build();
	}
	
	private List<FindAccountResponseModel.ContactUser> transferUsers(List<ContactUser> userList) {
		List<FindAccountResponseModel.ContactUser> results = new ArrayList<FindAccountResponseModel.ContactUser>();
		for (ContactUser u : userList) {
			FindAccountResponseModel.ContactUser transferResult = FindAccountResponseModel.ContactUser.builder()
					.accountId(u.getAccountId())
					.name(u.getName())
					.profile(transferProfile(u.getProfile()))
					.build();
			results.add(transferResult);
		}
		return results;
	}
	
	private FindAccountResponseModel.UserProfile transferProfile(UserProfile profile) {
		return FindAccountResponseModel.UserProfile.builder()
				.email(profile.getEmail())
				.phoneNumber(profile.getPhoneNumber())
				.build(); 
	}

}
