package com.cbx.tby.usecase.registration.contactuser.contract.find;

import com.cbx.tby.usecase.generic.contract.base.Request;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class FindContactUserRequest implements Request {
    private String id;
}
