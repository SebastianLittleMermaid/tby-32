package com.cbx.tby.usecase.registration.account.contract.repository;

import com.cbx.tby.model.registration.Account;
import com.cbx.tby.usecase.generic.contract.repository.Repository;

public interface AccountRepository extends Repository<Account> {

}
