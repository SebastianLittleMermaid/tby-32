package com.cbx.tby.usecase.registration.contactuser.impl.create.mapper;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.model.registration.UserProfile;
import com.cbx.tby.usecase.generic.contract.base.TransferMapper;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.create.CreateContactUserRequestModel;


public class CreateContactUserMapper implements TransferMapper<CreateContactUserRequest, ContactUser> {
    @Override
    public ContactUser transfer(final CreateContactUserRequest request) {
        final CreateContactUserRequestModel.UserProfile profile = request.getContactUser().getProfile();
        final UserProfile userProfile = UserProfile.builder()
                .email(profile.getEmail())
                .build();
        final ContactUser user = ContactUser.builder()
                .accountId(request.getContactUser().getAccountId())
                .name(request.getContactUser().getName())
                .profile(userProfile)
                .build();
        return user;
    }
}
