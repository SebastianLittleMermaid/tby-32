package com.cbx.tby.usecase.registration.account.contract.find;

import com.cbx.tby.usecase.generic.contract.base.Story;

public interface FindAccountUserStory extends Story<FindAccountRequest, FindAccountResponse> {

}
