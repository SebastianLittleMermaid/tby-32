package com.cbx.tby.usecase.mail.contract.send;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

public interface SendMailRequestModel {

    @ToString
    @Getter
    @Setter
    @Builder
    @JsonDeserialize(builder = SendMailRequestModel.Mail.MailBuilder.class)
    class Mail {
        private String subject;
        private String content;
        private List<String> sendTo;
        private String sendFrom;
        private Date sendTime;

        @JsonPOJOBuilder(withPrefix = "")
        public static final class MailBuilder {
        }
    }
}
