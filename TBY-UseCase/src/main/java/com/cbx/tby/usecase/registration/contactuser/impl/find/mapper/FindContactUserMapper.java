package com.cbx.tby.usecase.registration.contactuser.impl.find.mapper;

import com.cbx.tby.model.registration.ContactUser;
import com.cbx.tby.usecase.generic.contract.base.ReserseMapper;
import com.cbx.tby.usecase.generic.contract.base.TransferMapper;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserRequest;
import com.cbx.tby.usecase.registration.contactuser.contract.find.FindContactUserResponseModel;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

public class FindContactUserMapper
        implements TransferMapper<FindContactUserRequest, ContactUser>,
        ReserseMapper<FindContactUserResponseModel.ContactUser, ContactUser>{

    @Override
    public ContactUser transfer(final FindContactUserRequest request) {
        final ContactUser user = ContactUser.builder()
                .accountId(request.getId())
                .build();
        return user;
    }

    @Override
    public FindContactUserResponseModel.ContactUser reverse(final ContactUser contactUser) {
        if (contactUser == null) {
            return FindContactUserResponseModel.ContactUser.builder().build();
        }

        final String email = Optional.of(contactUser.getProfile().getEmail()).orElse(StringUtils.EMPTY);
        final FindContactUserResponseModel.UserProfile userProfile = FindContactUserResponseModel
                .UserProfile.builder()
                .email(email)
                .build();
        final FindContactUserResponseModel.ContactUser user = FindContactUserResponseModel.ContactUser.builder()
                .id(contactUser.getId())
                .accountId(contactUser.getAccountId())
                .name(contactUser.getName())
                .profile(userProfile)
                .build();
        return user;
    }
}
