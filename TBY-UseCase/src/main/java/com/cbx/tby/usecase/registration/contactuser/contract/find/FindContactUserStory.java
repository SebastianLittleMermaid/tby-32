package com.cbx.tby.usecase.registration.contactuser.contract.find;

import com.cbx.tby.usecase.generic.contract.base.Story;

public interface FindContactUserStory extends Story<FindContactUserRequest, FindContactUserResponse> {

}
