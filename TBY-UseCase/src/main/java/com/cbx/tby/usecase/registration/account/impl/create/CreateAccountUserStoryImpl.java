package com.cbx.tby.usecase.registration.account.impl.create;

import com.cbx.tby.model.mail.Mail;
import com.cbx.tby.model.registration.Account;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequest;
import com.cbx.tby.usecase.mail.contract.send.SendMailRequestModel;
import com.cbx.tby.usecase.mail.contract.send.SendMailResponse;
import com.cbx.tby.usecase.mail.contract.send.SendMailStory;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountRequest;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountResponse;
import com.cbx.tby.usecase.registration.account.contract.create.CreateAccountUserStory;
import com.cbx.tby.usecase.registration.account.contract.repository.AccountRepository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CreateAccountUserStoryImpl implements CreateAccountUserStory {

	private AccountRepository accountRepository;
	private CreateAccountTransferMapper mapper;
	private SendMailStory sendMail;
	
    public CreateAccountUserStoryImpl(AccountRepository repository, SendMailStory sendMail,
            CreateAccountTransferMapper mapper) {
		this.accountRepository = repository;
		this.sendMail = sendMail;
		this.mapper = mapper;
	}
	
	@Override
	public void execute(CreateAccountRequest request, CreateAccountResponse response) {
		// add validator
		Account account = mapper.transfer(request);
		String accountId = accountRepository.insert(account);
		SendMailRequest sendMailRequest = new SendMailRequest();
		
        List<String> emailList = account.getUser()
                                        .stream()
                                        .map(u -> u.getProfile().getEmail())
                                        .collect(Collectors.toList());
		
		SendMailRequestModel.Mail mail = SendMailRequestModel.Mail.builder()
                .sendFrom("tby@cbx.com")
                .sendTo(emailList)
                .sendTime(new Date())
                .content("registration success")
                .build();
		sendMailRequest.setMail(mail);
		sendMail.execute(sendMailRequest, new SkipSendMailResponse());
		response.response(accountId);
	}
	
	class SkipSendMailResponse implements SendMailResponse {
        @Override
        public void response(Mail data) {
            // do nothing
        }
	    
	}

}
