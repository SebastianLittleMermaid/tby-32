package com.cbx.tby.usecase.mail.contract.send;

import com.cbx.tby.usecase.generic.contract.base.Request;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SendMailRequest implements Request {
    private SendMailRequestModel.Mail mail;
}
